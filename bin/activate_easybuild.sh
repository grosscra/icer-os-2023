#!/bin/bash

# Set to your desired locations
: "${OS_CONTAINER:=$SCRATCH/icer-os-2023_latest.sif}"
: "${CONTAINER_OPT:=$SCRATCH/ubuntu/easybuild/opt}"

# https://stackoverflow.com/a/246128
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
ROOT_DIR="${SCRIPT_DIR}/.."

# Configuration files
CONTAINER_BASHRC="${ROOT_DIR}/config/easybuild/bashrc"
EASYBUILD_CONFIGFILES="${ROOT_DIR}/config/easybuild/config.cfg"

# Create location for software stack
mkdir -p "${CONTAINER_OPT}"

# Set env variables to be passed into the container
export SINGULARITYENV_USER="$USER"
export SINGULARITYENV_SCRATCH="$SCRATCH"
export SINGULARITYENV_PS1="[\u@\h \W]S "
# Known to EasyBuild, will use this config file by default
export SINGULARITYENV_EASYBUILD_CONFIGFILES="$EASYBUILD_CONFIGFILES"

singularity exec \
	--cleanenv \
	--bind "${CONTAINER_OPT}":/opt \
	"${OS_CONTAINER}" \
	/bin/bash --rcfile "${CONTAINER_BASHRC}"
