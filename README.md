# Container image for software on MSU ICER's next HPCC OS

This image is for the preparation of ICER's OS upgrade in 2023-2024 and is
based on Ubuntu 22.04. It contains an LMOD installation, and has the
appropriate dependencies for an EasyBuild install.

This image should be appropriate for building and running the user-facing
software stack, accessible through a new copy of `/opt/modules`.

## Quickstart

### EasyBuild

1. Pull down the container to the desired location (e.g., `$SCRATCH`):

    ``` bash
    cd $SCRATCH
    singularity pull docker://grosscra/icer-os-2023
    ```
2. (Optional) Set the following environment variables to the container location and where
   you want to install EasyBuild:

    ``` bash
    export OS_CONTAINER="$SCRATCH/icer-os-2023_latest.sif"
    export CONTAINER_OPT="$SCRATCH/ubuntu/easybuild/opt"
    ```

    If not set, these values will be used by default.

3. Start the container with `bin/activate_easybuild.sh`.
4. If not done already, [install EasyBuild](#installation).

## Building the container

**Note**: All docker commands must be run locally as super-user.

``` bash
cd /path/to/icer-os-2023/
sudo docker build -t icer-os-2023 .
```

## Running the container (locally)

``` bash
sudo docker run -it nextos bash
source /etc/profile  # access module command
```

## Pushing the container image

``` bash
sudo docker image tag icer-os-2023 grosscra/icer-os-2023:latest
sudo docker image push grosscra/icer-os-2023:latest
```

The resulting image will be pushed to [this Docker Hub
repository](https://hub.docker.com/repository/docker/grosscra/icer-os-2023/general).

## Running on the HPCC

Pull down the container:

``` bash
singularity pull docker://grosscra/icer-os-2023
```

Run the container (note that `--cleanenv` is necessary to avoid conflicts with
the existing module system on the HPCC):

``` bash
singularity shell --cleanev icer-os-2023_latest.sif
source /etc/profile  # access module command
```

Alternatively, to avoid needing to source `/etc/profile`, you can start `bash`
as a login shell:

``` bash
singularity exec --cleanenv icer-os-2023_latest.sif /bin/bash --login
```

## EasyBuild

### Installation

We will follow [the steps for installing EasyBuild using
EasyBuild](https://docs.easybuild.io/installation/#eb_as_module). First, invoke
the container with the desired binding for `/opt`:

``` bash
singularity exec \
    --bind /fake/opt/here:/opt \
    icer-os-2023_latest.sif \
    /bin/bash --login
```

Then, we install a temporary copy of EasyBuild:

``` bash
export EB_TMPDIR=/tmp/$USER/eb_tmp
python3 -m pip install --ignore-installed --prefix $EB_TMPDIR easybuild
```

Now, we update the environment variables to be able to run the temporary copy:

``` bash
export PATH=$EB_TMPDIR/local/bin:$PATH
export PYTHONPATH=$(/bin/ls -rtd -1 $EB_TMPDIR/local/lib*/python*/dist-packages | tail -1):$PYTHONPATH
export EB_PYTHON=python3
```

Now, we install EasyBuild in the bind-mounted `/opt`:

``` bash
eb --install-latest-eb-release --prefix /opt
```

EasyBuild is now installed in `/fake/opt/here` on the host and `/opt` in the
container.

### Usage

For now, from within the container, add `/opt/software/all` to `$MODULEPATH` to
be able to find it:

``` bash
module use "$MODULE_ROOT/all"
module load EasyBuild
```
