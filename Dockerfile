FROM ubuntu:22.04
ENV DEBIAN_FRONTEND=noninteractive
RUN apt update \
&& apt install -y curl python3 python3-distutils python3-pip \
&& apt install -y lmod
# debianutils provides 'which' command
RUN apt install -y bzip2 debianutils diffutils file gcc g++ git gzip libibverbs-dev openssl libssl-dev make patch sudo tar unzip xz-utils
RUN python3 -m pip install archspec
COPY lmod.sh /etc/profile.d/lmod.sh
